//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#pragma comment(lib, "RsUtil-Berlin.lib")

#include <set>
#include <vector>
#include <boost/foreach.hpp>
#include <System.NetEncoding.hpp>
#include <Registry.hpp>
#include <shlobj.h>
#include <RsUtil.h>

#include "FrmMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------

const HKEY PuttyRootKey = HKEY_CURRENT_USER;
const wchar_t PuttySessionsKey[] = L"Software\\SimonTatham\\PuTTY\\Sessions";

std::vector<String> GetSessions()
{
  std::vector<String> Sessions;
  std::unique_ptr<TRegistry> Reg(new TRegistry);
  Reg->RootKey = PuttyRootKey;
  if (!Reg->OpenKeyReadOnly(PuttySessionsKey))
    return Sessions;
  std::unique_ptr<TStrings> Keys(new TStringList);
  Reg->GetKeyNames(Keys.get());
  for (int i = 0; i < Keys->Count; ++i)
    Sessions.push_back(TNetEncoding::URL->Decode(Keys->Strings[i]));
  return Sessions;
}

std::set<String> GetAllSettings()
{
  std::vector<String> Sessions = GetSessions();
  std::set<String> Settings;
  std::unique_ptr<TRegistry> Reg(new TRegistry);
  Reg->RootKey = PuttyRootKey;
  BOOST_FOREACH (const String &Session, Sessions)
    {
    if (!Reg->OpenKeyReadOnly(rsutil::JoinPaths(PuttySessionsKey, Session)))
      continue;
    std::unique_ptr<TStrings> ValueNames(new TStringList);
    Reg->GetValueNames(ValueNames.get());
    for (int i = 0; i < ValueNames->Count; ++i)
      Settings.insert(ValueNames->Strings[i]);
    }
  return Settings;
}

//---------------------------------------------------------------------------
__fastcall TFormMain::TFormMain(TComponent* Owner)
  : TForm(Owner)
{
  std::vector<String> Sessions = GetSessions();
  BOOST_FOREACH (const String &Session, Sessions)
    {
    ListBoxFrom->Items->Append(Session);
    CheckListBoxTo->Items->Append(Session);
    }
  std::set<String> Settings = GetAllSettings();
  BOOST_FOREACH (const String &Setting, Settings)
    {
    CheckListBoxSettings->Items->Append(Setting);
    }
}
//---------------------------------------------------------------------------

static String FindPuttyExe()
{
  const wchar_t *EnvKeys[] = { L"ProgramFiles", L"ProgramFiles(x86)", L"ProgramW6432" };
  std::set<String> ProgsDirs;
  BOOST_FOREACH (const wchar_t *EnvKey, EnvKeys)
    {
    const wchar_t *ProgsDir = _wgetenv(EnvKey);
    if (ProgsDir)
      ProgsDirs.insert(ProgsDir);
    }
  BOOST_FOREACH (const String &ProgsDir, ProgsDirs)
    {
    const String PuttyExe = rsutil::JoinPaths(ProgsDir, L"PuTTY", L"putty.exe");
    if (FileExists(PuttyExe))
      return PuttyExe;
    }
  throw Exception(L"Could not find putty.exe");
}

static String StartPuttySession(const String &Session)
{
  String PuttyExe = FindPuttyExe();

  String CmdLine = rsutil::PrintF(L"\"%ls\" -load \"%ls\"",
    PuttyExe.c_str(),
    Session.c_str());

  STARTUPINFOW StartupInfo;
  memset(&StartupInfo, 0, sizeof(StartupInfo));
  StartupInfo.cb = sizeof(StartupInfo);
  PROCESS_INFORMATION ProcInfo;
  BOOL rv = CreateProcessW(
    NULL,
    CmdLine.c_str(),
    NULL,
    NULL,
    FALSE,
    0,
    NULL,
    NULL,
    &StartupInfo,
    &ProcInfo);
  if (rv)
    {
    CloseHandle(ProcInfo.hThread);
    CloseHandle(ProcInfo.hProcess);
    }
}

void __fastcall TFormMain::ButtonStartSelectedFromSessionClick(TObject *Sender)
{
  if (ListBoxFrom->ItemIndex >= 0)
    StartPuttySession(ListBoxFrom->Items->Strings[ListBoxFrom->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonStartSelectedToSessionsClick(TObject *Sender)
{
  for (int i = 0; i < CheckListBoxTo->Items->Count; ++i)
    {
    if (CheckListBoxTo->Checked[i])
      StartPuttySession(CheckListBoxTo->Items->Strings[i]);
    }
}
//---------------------------------------------------------------------------

static void SetAllChecks(TCheckListBox *CheckListBox, bool Checked)
{
  CheckListBox->Items->BeginUpdate();
  for (int i = 0; i < CheckListBox->Items->Count; ++i)
    CheckListBox->Checked[i] = Checked;
  CheckListBox->Items->EndUpdate();
}

static void SelectAll(TCheckListBox *CheckListBox)
{
  SetAllChecks(CheckListBox, true);
}

static void SelectNone(TCheckListBox *CheckListBox)
{
  SetAllChecks(CheckListBox, false);
}

//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSelectAllToClick(TObject *Sender)
{
  SelectAll(CheckListBoxTo);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSelectNoneToClick(TObject *Sender)
{
  SelectNone(CheckListBoxTo);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSelectAllSettingsClick(TObject *Sender)
{
  SelectAll(CheckListBoxSettings);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSelectNoneSettingsClick(TObject *Sender)
{
  SelectNone(CheckListBoxSettings);
}
//---------------------------------------------------------------------------

static std::vector<String> GetCheckedItems(TCheckListBox *CheckListBox)
{
  std::vector<String> Items;
  for (int i = 0; i < CheckListBox->Items->Count; ++i)
    {
    if (CheckListBox->Checked[i])
      Items.push_back(CheckListBox->Items->Strings[i]);
    }
  return Items;
}

struct TRegKey
{
  HKEY hKey;

  TRegKey(HKEY hRootKey, const String &Key, REGSAM DesiredAccess)
  {
    LSTATUS rv = RegOpenKeyExW(hRootKey, Key.c_str(), 0, DesiredAccess, &hKey);
    if (rv != ERROR_SUCCESS)
      throw Exception(rsutil::PrintF(L"Failed to open registry key \"%s\"", Key.c_str()));
  }

  ~TRegKey()
  {
    RegCloseKey(hKey);
  }
};

static void CopySettings(const String &SrcSession, const String &DstSession, const std::vector<String> &Settings)
{
  TRegKey SrcKey(PuttyRootKey, rsutil::JoinPaths(PuttySessionsKey, TNetEncoding::URL->EncodeQuery(SrcSession)), KEY_READ);
  TRegKey DstKey(PuttyRootKey, rsutil::JoinPaths(PuttySessionsKey, TNetEncoding::URL->EncodeQuery(DstSession)), KEY_WRITE);
  BOOST_FOREACH (const String &Setting, Settings)
    {
    DWORD Type;
    DWORD Size;
    LSTATUS rv = RegGetValueW(SrcKey.hKey, L"", Setting.c_str(), RRF_RT_ANY | RRF_NOEXPAND, &Type, NULL, &Size);
    if (rv == ERROR_SUCCESS)
      {
      std::vector<char> Buffer(Size);
      rv = RegGetValueW(SrcKey.hKey, L"", Setting.c_str(), RRF_RT_ANY | RRF_NOEXPAND, &Type, &Buffer[0], &Size);
      if (rv != ERROR_SUCCESS)
        throw Exception(rsutil::PrintF(L"Failed to read value \"%s\" in source session key \"%s\"", Setting.c_str(), SrcSession.c_str()));
      rv = RegSetKeyValueW(DstKey.hKey, L"", Setting.c_str(), Type, &Buffer[0], Size);
      if (rv != ERROR_SUCCESS)
        throw Exception(rsutil::PrintF(L"Failed to write value \"%s\" in destination session key \"%s\"", Setting.c_str(), DstSession.c_str()));
      }
    else if (rv == ERROR_FILE_NOT_FOUND)
      {
      LSTATUS rv = RegDeleteValueW(DstKey.hKey, Setting.c_str());
      }
    else
      {
      // TODO: error handling
      }
    }
}

void __fastcall TFormMain::ButtonSyncClick(TObject *Sender)
{
  if (ListBoxFrom->ItemIndex < 0)
    return;
  String SrcSession = ListBoxFrom->Items->Strings[ListBoxFrom->ItemIndex];
  std::vector<String> DstSessions = GetCheckedItems(CheckListBoxTo);
  std::vector<String> Settings = GetCheckedItems(CheckListBoxSettings);
  BOOST_FOREACH (const String &DstSession, DstSessions)
    {
    if (DstSession != SrcSession)
      CopySettings(SrcSession, DstSession, Settings);
    }
  ShowMessage(L"Done");
}
//---------------------------------------------------------------------------

