object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'Sync PuTTY settings'
  ClientHeight = 578
  ClientWidth = 855
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    855
    578)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 4
    Width = 28
    Height = 13
    Caption = 'From:'
  end
  object Label2: TLabel
    Left = 295
    Top = 4
    Width = 16
    Height = 13
    Caption = 'To:'
  end
  object Settings: TLabel
    Left = 583
    Top = 4
    Width = 39
    Height = 13
    Caption = 'Settings'
  end
  object ListBoxFrom: TListBox
    Left = 7
    Top = 23
    Width = 265
    Height = 457
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object CheckListBoxTo: TCheckListBox
    Left = 295
    Top = 23
    Width = 265
    Height = 457
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 2
  end
  object CheckListBoxSettings: TCheckListBox
    Left = 583
    Top = 23
    Width = 265
    Height = 457
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 6
  end
  object ButtonSync: TButton
    Left = 390
    Top = 544
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Sync'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = ButtonSyncClick
  end
  object ButtonSelectAllTo: TButton
    Left = 488
    Top = 486
    Width = 33
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'All'
    TabOrder = 4
    OnClick = ButtonSelectAllToClick
  end
  object ButtonSelectNoneTo: TButton
    Left = 527
    Top = 486
    Width = 33
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'None'
    TabOrder = 5
    OnClick = ButtonSelectNoneToClick
  end
  object ButtonSelectNoneSettings: TButton
    Left = 815
    Top = 486
    Width = 33
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'None'
    TabOrder = 7
    OnClick = ButtonSelectNoneSettingsClick
  end
  object ButtonSelectAllSettings: TButton
    Left = 776
    Top = 486
    Width = 33
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'All'
    TabOrder = 8
    OnClick = ButtonSelectAllSettingsClick
  end
  object ButtonStartSelectedFromSession: TButton
    Left = 7
    Top = 486
    Width = 169
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Start selected session'
    TabOrder = 1
    OnClick = ButtonStartSelectedFromSessionClick
  end
  object ButtonStartSelectedToSessions: TButton
    Left = 295
    Top = 486
    Width = 169
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Start selected session(s)'
    TabOrder = 3
    OnClick = ButtonStartSelectedToSessionsClick
  end
end
