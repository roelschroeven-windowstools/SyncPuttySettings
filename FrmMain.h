//---------------------------------------------------------------------------

#ifndef FrmMainH
#define FrmMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.CheckLst.hpp>
//---------------------------------------------------------------------------
class TFormMain : public TForm
{
__published:	// IDE-managed Components
  TListBox *ListBoxFrom;
  TLabel *Label1;
  TLabel *Label2;
  TCheckListBox *CheckListBoxTo;
  TCheckListBox *CheckListBoxSettings;
  TLabel *Settings;
  TButton *ButtonSync;
  TButton *ButtonSelectAllTo;
  TButton *ButtonSelectNoneTo;
  TButton *ButtonSelectNoneSettings;
  TButton *ButtonSelectAllSettings;
  TButton *ButtonStartSelectedFromSession;
  TButton *ButtonStartSelectedToSessions;
  void __fastcall ButtonStartSelectedFromSessionClick(TObject *Sender);
  void __fastcall ButtonStartSelectedToSessionsClick(TObject *Sender);
  void __fastcall ButtonSelectAllToClick(TObject *Sender);
  void __fastcall ButtonSelectNoneToClick(TObject *Sender);
  void __fastcall ButtonSelectAllSettingsClick(TObject *Sender);
  void __fastcall ButtonSelectNoneSettingsClick(TObject *Sender);
  void __fastcall ButtonSyncClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
